<?php
/**
 * Telegram bot Rob Geus
 *
 * PHP Version 5
 *
 * @category Bot
 * @package  RobGeus
 * @author   Joël Morren <jo.morren@gmail.com>
 * @license  (c) 2015
 */

require_once __DIR__.'/../lib/class.RobGeusMessage.php';
require_once __DIR__.'/../lib/class.RobGeusPhoto.php';
$array = json_decode(file_get_contents('php://input'), true);

if(isset($array['message']['text'])) {
	$RGBM = new RobGeusMessage();
	$RGBM->main($array);
} elseif(isset($array['message']['photo']) && strcmp($array['message']['chat']['id'], '127001187') == 0) {
	$RGBP = new RobGeusPhoto();
	$RGBP->main($array);
} else {
	error_log(var_export(json_decode(file_get_contents('php://input'), true), true));
}
