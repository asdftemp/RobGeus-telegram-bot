<?php
/**
 * Telegram bot Rob Geus
 *
 * PHP Version 5
 *
 * @category Bot
 * @package  RobGeusSetWebHook
 * @author   Joël Morren <jo.morren@gmail.com>
 * @license  (c) 2015
 */

require_once __DIR__.'/../lib/class.setWebHook.php';

$RGBP = new setWebHook();
$RGBP->main();
