<?php
/**
* RobGeusPhoto
*
* PHP Version 5
*
* @category Lib
* @package  RobGeusPhoto
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015 
*/

require_once __DIR__.'/class.Mysql.php';
/** RobGeusPhoto class
*
* @category Lib
* @package  RobGeusPhoto
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015
*/

class RobGeusPhoto {
	public $API_KEY  = '199637198:AAFyyJEuw3rpxQberVNs2AdvRP6tU_lXloQ';
	public $BOT_NAME = 'RobGeus_bot';
	public $message;

	/**
	 * main class
	 *
	 * @param string $json json input form telegrama
	 * @return void
	 */
	public function main($array) {
		$this->proccesPhoto($array);
	}

	/**
	 * procces word
	 *
	 *	@returns number of found words
	 */
	public function proccesPhoto($array) {
		$array = $this->genArray($array);

		$this->sendToDB($array);
	}

	/**
	 *
	 *
	 *
	 */
	public function genArray($array) {
		$count = $this->getArrayCount();
		$array = array ( 
			'Count' => $array['message']['caption'],
			'PhotoID' => $array['message']['photo'][$count]['file_id']
		);

		return $array;
	}

	/**
	 *
	 *
	 *
	 */
	public function getArrayCount() {
		$array = $this->message['message']['photo'];
		$count = count($array);

		if ($count >= 1) {
			return 0;
		} else {
			return $count--;
		}
		
		
	}

	/**
	 *
	 *
	 *
	 */
	public function sendToDB($array) {
		$mysql = new Mysql();
		$mysql->procesArray($array);
		$mysql->addToDB();
	}
}

