<?php
/**
* Mysql
*
* PHP Version 5
*
* @category Lib
* @package  Mysql
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015
*/

/** Mysql class
*
* @category Lib
* @package  Mysql
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015
*/
class setWebHook{
    public $url      = 'https://telegram.een.kiwi/';
    public $token    = '199637198:AAFyyJEuw3rpxQberVNs2AdvRP6tU_lXloQ';
    public $path     = '/src/html/hook.php';
    public $hook_url = $url . $token . $path;

    /**
     * main class
     * 
     * @return void
     */
    public function main() {

    }

    /**
     * Send messages
     *
     * @param string $number number of matches
     * @return true/false
     */
    public function sendMessage ($number) {
        $json   = $this->genJson($number);
        $handle = $this->setUpCurl($json);

        $this->execCurl($handle);
    }

    /**
     * close the cURL
     *
     * @param handle $handle curl handle
     */
    public function closeCurl($handle) {
        curl_close($handle);
    }

    public function setUpCurl($json) {
    $handle = curl_init($this->genUrl('setWebHook'));

    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);
    curl_setopt($handle, CURLOPT_POSTFIELDS, $this->hook_url);
    curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

    return $handle;
    }

}
