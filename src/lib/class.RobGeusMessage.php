<?php
/**
* RobGeusMessage
*
* PHP Version 5
*
* @category Lib
* @package  RobGeusMessage
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015 
*/

/** RobGeusMessage class
*
* @category Lib
* @package  RobGeusMessage
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015
*/

class RobGeusMessage {
	public $API_KEY  = '199637198:AAFyyJEuw3rpxQberVNs2AdvRP6tU_lXloQ';
	public $BOT_NAME = 'RobGeus_bot';
	public $message;
	public $wordCount = 0;

	/**
	 * main class
	 *
	 * @param string $json json input form telegrama
	 * @return void
	 */
	public function main($array) {
		$this->message = $array;

		$this->proccesText();
	}
/*
	/**
	 * decodeJson
	 *
	 * @param string $json json ouput form telegrama
	 * @return void
	 
	public function decodeJson($json) {
		if($this->message = json_decode($json, true)) {
			return true;
		} else {
			return false; //error
		}
	}
*/
	/**
	 * procces word
	 *
	 *	@returns number of found words
	 */
	public function proccesText() {
		$text = $this->message['message']['text'];
		$textArray = preg_split('/[^a-z0-9]/i', $text);

		foreach ($textArray as $v) {
			if ($this->countWords($v)) {
				if ($this->wordCount >= 2) {
					//$this->sendMessage();
					$this->sendPhoto();
					$this->wordCount = 0;
				}
			} else {
					$this->wordCount = 0;
			}
		}
	}

	/**
	 *	count words
	 *
	 * @return word count in string
	 */
	public function countWords($text) {
		$lowerCase = $this->lowerCase($text);
		$count     = substr_count($lowerCase, 'man');

		$this->wordCount += $count;

		if ($count >= 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * makes all words lowercase
	 *
	 * @param string $text input to make all lowercase
	 * @return lowercase string
	 */
	public function lowerCase($string) {
		$rv = strtolower($string);

		return $rv;
	}

	/**
	 * Send messages
	 *
	 * @return true/false
	 */
	public function sendMessage () {
		$json   = $this->genJson($this->wordCount);
		$handle = $this->setUpCurl($json);

		$this->execCurl($handle);
	}

	/**
	 * execute the cURL
	 *
	 * @param handle $handle curl handle
	 */
	public function execCurl($handle) {
		$response = curl_exec($handle);

		if (!$response) {
			$this->closeCurl($handle);
		}

		$this->handleResponse($response, $handle);
		$this->closeCurl($handle);
	}

	
	/**
	 * close the cURL
	 *
	 * @param handle $handle curl handle
	 */
	public function closeCurl($handle) {
		curl_close($handle);
	}

	/**
	 * handles http return code
	 *
	 * @param string $code http code
	 * @return true/false
	 */
	public function handleResponse($response, $handle) {
		$code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));

		if ($code >= 500) {
			sleep(10);
			return false;
		} elseif ($code != 200) {
			error_log(var_export($code, true));		//error
		} elseif ($code = 401) {
			error_log(var_export($code, true));		//error
		} else {
			$response = json_decode($response, true);
			if (isset($response['description'])) {
				return true;
			}
		}
	}

	public function setUpCurl($json) {
		$handle = curl_init($this->genUrl('sendPhoto'));

		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($handle, CURLOPT_TIMEOUT, 60);
		curl_setopt($handle, CURLOPT_POSTFIELDS, $json);
		curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

		return $handle;
	}

	public function genJson() {
		$message = array();
		$message['photo'] = $this->choosePhoto();
		$message['chat_id'] = $this->message['message']['chat']['id'];
		$message['disable_notification'] = true;
		$rv                             = json_encode($message);

		return $rv;
	}

	public function genUrl($method) {
		$baseurl  = 'https://api.telegram.org/bot';
		$url = $baseurl . $this->API_KEY .'/'. $method;

		return $url;
	}

	/**
	 * Send photo
	 *
	 * @return true/false
	 */
	public function sendPhoto () {
		$json   = $this->genJson();
		$handle = $this->setUpCurl($json);

		$this->execCurl($handle);
	}

	/**
	 * Get photo
	 *
	 * @return true/false
	 */
	public function getPhotos () {
		$array = array(
			'Count' => $this->wordCount,
			'PhotoID' => ''
		);
		$mysql = new Mysql();

		$mysql->procesArray($array);

		$rv    = $mysql->getPhotoID();

		return $rv;
	}

	/**
	 * Choose photo
	 *
	 * @return true/false
	 */
	public function choosePhoto () {
		$photoArray = $this->getPhotos();
		$choosen    = rand(0, count($photoArray) - 1);

		if ($choosen <= 0) {
			return $photoArray[0]['id'];
		} else {
			return $photoArray[$choosen]['id'];
		}
	}

}
