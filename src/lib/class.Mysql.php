<?php
/**
* Mysql
*
* PHP Version 5
*
* @category Lib
* @package  Mysql
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015 
*/

/** Mysql class
*
* @category Lib
* @package  Mysql
* @author   Joël Morren <jo.morren@gmail.com>
* @license  (c) 2015
*/

class Mysql {
	public $mysqli;
	public $array;

	/**
	 * __construct class
	 *
	 * @return void
	 */
	public function __construct() {
		$this->connect();
	}

	/**
	 * makes connection with db
	 *
	 * @return true or false
	 */
	public function connect() {
		$server  = 'localhost';
		$username = 'RobGeus';
		$password = 'RobTelegram';
		$dbname   = 'RobGeus';

		$this->mysqli = new mysqli($server, $username, $password, $dbname);
		
		if ($this->mysqli->connect_errno) {
			die('connection failed: '. $this->mysqli->connect_error);
		}

	}


	/**
	 * adds photo to db
	 *
	 * @return true or false
	 */
	public function procesArray($array) {
		$this->array = array_map('mysql_real_escape_string', $array);
	}
	/**
	 * adds photo to db
	 *
	 * @return true or false
	 */
	public function addToDB() {
    	$sql = "INSERT INTO telegram (count, id, accepted) VALUES ('" . $this->array['Count'] . "','" . $this->array['PhotoID'] . "', true);";

//		$sql  = "INSERT INTO RobGeus (count, id, accepted)";
//		$sql .= "VALUES ('" . $this->array[Count] . "', $this->array[PhotoID], true);";

		if (!$this->checkIfExists()) {
			if ($this->mysqli->query($sql)) {
				return true;
			} else {
				return false;
			}
		}

		$this->mysqli->close();
	}

	/**
	 * checks if photo is already in db
	 *
	 * @return true or false
	 */
	public function checkIfExists() {
    	$sql = "SELECT * FROM telegram WHERE id = '" . $this->array['PhotoID']. "'";
		$result = $this->mysqli->query($sql);

		if ($result->num_rows >= 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * returns PhotoID form DB
	 *
	 * @return array of PhotoID's
	 */
	public function getPhotoID() {
    	$sql    = "SELECT * FROM telegram WHERE count = '" . $this->array['Count']. "'";
		$result = $this->mysqli->query($sql);
		$rv     = array();

		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			array_push($rv, $row);
		}

		return $rv;
	}
}
